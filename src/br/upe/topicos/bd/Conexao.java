package br.upe.topicos.bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class Conexao {
	private static Conexao instancia;
	private final String usuario;
	private final String senha;
	private final String url;

	static {
		instancia = new Conexao();
	}

	private Conexao() {    	
		this.usuario = "root";
		this.senha = "managers,#7";
		this.url = "jdbc:mysql://localhost:3306/ti_app_web?useTimezone=true&serverTimezone=UTC&useSSL=false";
	}

	public static Conexao getInstancia() {
		return instancia;
	}

	public Connection open() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		return DriverManager.getConnection(this.url, this.usuario, this.senha);
	}

	public void close(ResultSet rs, Statement st, Connection conn) throws SQLException, ClassNotFoundException {
		if (rs != null) {
			rs.close();
		}
		if (st != null) {
			st.close();
		}
		if (conn != null) {
			conn.close();
		}
	}

	public void close(Statement st, Connection conn) throws SQLException, ClassNotFoundException {
		this.close(null, st, conn);
	}

	public void close(Connection conn) throws SQLException, ClassNotFoundException {
		this.close(null, null, conn);
	}
}