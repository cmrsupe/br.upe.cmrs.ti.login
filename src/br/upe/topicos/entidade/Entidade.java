package br.upe.topicos.entidade;

public interface Entidade<K> {
	K getId();
	void setId(K id);
}
