package br.upe.topicos.controlador;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.upe.topicos.entidade.usuario.Usuario;
import br.upe.topicos.servico.Fachada; 

@SuppressWarnings("serial")
@WebServlet(value="/autenticar")
public class AutenticarServlet extends HttpServlet{

	@SuppressWarnings("unused")
	private ServletConfig config;

	public void init(ServletConfig config) throws ServletException{
		this.config=config;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException,IOException{
		response.sendRedirect("index.jsp");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException,IOException{
		try {
			HttpSession session=request.getSession();
			session.setAttribute("msg", null);
			Usuario usuario = Fachada.getInstancia().getUsuarioServico().getPorAutenticacao(request.getParameter("login"), request.getParameter("senha"));
			if (usuario!=null) {
				this.prepararSucesso(session, response, usuario);
			}else {
				this.prepararFalha(session, request, response);
			}
		}catch(Exception e){
			System.out.println("Exception is ;"+e);
		}
	}
	
	private void prepararSucesso(HttpSession session,
			HttpServletResponse response, Usuario usuario) throws IOException {
		session.setAttribute("msg", "Sucesso na autenticação!");
		session.setAttribute("logado", usuario);
		response.sendRedirect("privado/logado.jsp");		
	}
	
	private void prepararFalha(HttpSession session,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session.setAttribute("msg", "Falha na autenticação!");
		request.getRequestDispatcher("index.jsp")
		.include(request, response);
	}
}