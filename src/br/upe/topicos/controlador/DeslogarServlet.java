package br.upe.topicos.controlador;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SuppressWarnings("serial")
@WebServlet(value="/deslogar")
public class DeslogarServlet extends HttpServlet{

	@SuppressWarnings("unused")
	private ServletConfig config;

	public void init(ServletConfig config) throws ServletException{
		this.config=config;
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException,IOException{
		//PrintWriter out = response.getWriter();
		//out.close();
		//response.setContentType("text/html");  
		try {
			HttpSession session=request.getSession();  
			session.setAttribute("msg", null);
			if (session.getAttribute("logado")!=null) {
				session.removeAttribute("logado");
				session.setAttribute("msg", "Deslogado com sucesso!");
			}
			response.sendRedirect("index.jsp");
		}catch(Exception e){
			System.out.println("Exception is ;"+e);
		}
	}
}