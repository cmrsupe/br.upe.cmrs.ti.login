package br.upe.topicos.servico.exceptions;

import br.upe.topicos.dao.exceptions.DAOException;

public class ServicoException extends DAOException {
	
	private static final long serialVersionUID = 1L;
	
	public ServicoException(String msg) {
		super(msg);
	}

	public ServicoException(DAOException t) {
		super(t);
	}

	public ServicoException(String msg, DAOException t) {
		super(msg, t);
	}
}