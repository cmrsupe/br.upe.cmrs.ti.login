package br.upe.topicos.servico;

import br.upe.topicos.servico.usuario.UsuarioServico;

public class Fachada {
	private static final Fachada instancia;
	private final UsuarioServico usuarioServico;
	
	static {
		instancia = new Fachada();
	}
	
	private Fachada() {
		this.usuarioServico = new UsuarioServico();
	}

	public static Fachada getInstancia() {
		return instancia;
	}

	public UsuarioServico getUsuarioServico() {
		return usuarioServico;
	}
}
