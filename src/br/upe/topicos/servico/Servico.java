package br.upe.topicos.servico;

import java.io.Serializable;
import java.util.List;

import br.upe.topicos.dao.exceptions.DAOException;
import br.upe.topicos.entidade.Entidade;
import br.upe.topicos.servico.exceptions.ServicoException;

public interface Servico<T extends Entidade<K>, K extends Serializable> {
	T getPorId(K id) throws DAOException, ServicoException;
	T persistir(T entidade) throws DAOException, ServicoException;
	int deletar(@SuppressWarnings("unchecked") K... ids) throws DAOException, ServicoException;	
	T getPorAtributo(String propriedade, Object valor) throws DAOException, ServicoException;
	List<T> listar(String propriedade, Object valor, String orderBy) throws DAOException, ServicoException;
	List<T> listarTodos(String orderBy) throws DAOException, ServicoException;
	void validarCampos(T entidade) throws DAOException, ServicoException;
}
