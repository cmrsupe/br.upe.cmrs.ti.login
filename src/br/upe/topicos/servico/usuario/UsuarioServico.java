package br.upe.topicos.servico.usuario;

import java.util.List;

import br.upe.topicos.dao.Atributo;
import br.upe.topicos.dao.exceptions.DAOException;
import br.upe.topicos.dao.usuario.UsuarioDAO;
import br.upe.topicos.entidade.usuario.Usuario;
import br.upe.topicos.servico.Servico;
import br.upe.topicos.servico.exceptions.ServicoException;

public class UsuarioServico implements Servico<Usuario, Long>{

	private final UsuarioDAO dao;

	public UsuarioServico() {
		this.dao = new UsuarioDAO();
	}

	@Override
	public void validarCampos(Usuario entidade) throws DAOException, ServicoException {
		String sql = null;
		if (entidade.getId()!=null) {
			sql = "select * from usuarios where login ='" + entidade.getLogin() + "' and id <> " + entidade.getId();
		}else {
			sql = "select * from usuarios where login ='" + entidade.getLogin() + "'";
		}
		List<Usuario> resultado = this.dao.pesquisar(sql);
		if (!resultado.isEmpty()) {
			throw new ServicoException("J� existe um usu�rio com o login " + entidade.getLogin() + ".");
		}
	}

	@Override
	public Usuario getPorId(Long id) throws DAOException, ServicoException {
		List<Usuario> resultado = this.dao.pesquisar(1, null, new Atributo("id", id));
		if (resultado.isEmpty()) {
			return null;
		}else {
			return resultado.get(0);
		}
	}

	@Override
	public Usuario persistir(Usuario entidade) throws DAOException, ServicoException {
		this.validarCampos(entidade);		
		if (entidade.getId()==null) {
			this.dao.inserir(entidade);
		}else {
			this.dao.atualizar(entidade);
		}
		return entidade;
	}

	@Override
	public int deletar(Long... ids) throws DAOException, ServicoException {
		return this.dao.deletar(ids);
	}

	@Override
	public Usuario getPorAtributo(String propriedade, Object valor) throws DAOException, ServicoException {
		List<Usuario> resultado = this.dao.pesquisar(1, null, new Atributo(propriedade, valor));
		if (resultado.isEmpty()) {
			return null;
		}else {
			return resultado.get(0);
		}
	}

	@Override
	public List<Usuario> listar(String propriedade, Object valor, String orderBy) throws DAOException, ServicoException {
		return this.dao.pesquisar(0, orderBy, new Atributo(propriedade, valor));
	}

	@Override
	public List<Usuario> listarTodos(String orderBy) throws DAOException, ServicoException {
		return this.dao.pesquisar("select * from usuarios order by " + orderBy);
	}

	public Usuario getPorAutenticacao(String login, String senha) throws DAOException, ServicoException {
		List<Usuario> resultado = this.dao.pesquisar(1, null, new Atributo("login", login), new Atributo("senha", senha));
		if (resultado.isEmpty()) {
			return null;
		}else {
			return resultado.get(0);
		}		
	}
}
