package br.upe.topicos.dao.usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.upe.topicos.bd.Conexao;
import br.upe.topicos.dao.Atributo;
import br.upe.topicos.dao.DAO;
import br.upe.topicos.dao.exceptions.DAOException;
import br.upe.topicos.entidade.usuario.Usuario;

public class UsuarioDAO implements DAO<Usuario, Long> {

	private static final Logger logger = Logger.getLogger(UsuarioDAO.class.getName());  

	public UsuarioDAO() {
	}

	@Override
	public Usuario inserir(Usuario entidade) throws DAOException {
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = Conexao.getInstancia().open();
			String sql = "insert into usuarios(login, senha, nome, email) values(?, ?, ?, ?)";
			st = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);			
			st.setString(1, entidade.getLogin());
			st.setString(2, entidade.getSenha());
			st.setString(3, entidade.getNome());	
			st.setString(4, entidade.getEmail());
			int linhasRealizadas = st.executeUpdate();
			if (linhasRealizadas == 0) {
				throw new DAOException(new SQLException("Nenhum registro foi criado."));
			}
			try (ResultSet chaveGerada = st.getGeneratedKeys()) {
				if (chaveGerada.next()) {
					entidade.setId(chaveGerada.getLong(1));
				}
				else {
					throw new DAOException(new SQLException("N�o foi poss�vel obter o ID gerado."));
				}
			}
			logger.log(Level.INFO, "Dados inseridos com sucesso.");
			return entidade;
		}catch(SQLException | ClassNotFoundException ex) {
			if (ex instanceof SQLException) {
				throw new DAOException((SQLException)ex);
			}else {
				throw new DAOException((ClassNotFoundException)ex);
			}
		}finally {
			try {
				Conexao.getInstancia().close(rs, st, conn);
			}catch(SQLException | ClassNotFoundException ex) {
				if (ex instanceof SQLException) {
					throw new DAOException((SQLException)ex);
				}else {
					throw new DAOException((ClassNotFoundException)ex);
				}
			}
		}	
	}

	@Override
	public Usuario atualizar(Usuario entidade) throws DAOException {
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = Conexao.getInstancia().open();
			String sql = "update usuarios set login=?, senha=?, nome=?, email=? where id=?";
			st = conn.prepareStatement(sql);			
			st.setString(1, entidade.getLogin());
			st.setString(2, entidade.getSenha());
			st.setString(3, entidade.getNome());	
			st.setString(4, entidade.getEmail());
			st.setLong(5, entidade.getId());
			int linhasRealizadas = st.executeUpdate();
			if (linhasRealizadas == 0) {
				logger.log(Level.WARNING, "Nenhum registro foi atualizado.");
			}else {
				logger.log(Level.INFO, linhasRealizadas + " registro(s) atualizado(s).");
			}
			return entidade;
		}catch(SQLException | ClassNotFoundException ex) {
			if (ex instanceof SQLException) {
				throw new DAOException((SQLException)ex);
			}else {
				throw new DAOException((ClassNotFoundException)ex);
			}
		}finally {
			try {
				Conexao.getInstancia().close(rs, st, conn);
			}catch(SQLException | ClassNotFoundException ex) {
				if (ex instanceof SQLException) {
					throw new DAOException((SQLException)ex);
				}else {
					throw new DAOException((ClassNotFoundException)ex);
				}
			}
		}	
	}

	@Override
	public int deletar(Long... ids) throws DAOException {
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			conn = Conexao.getInstancia().open();
			StringBuilder sb = new StringBuilder("0");
			for (Long l : ids) {
				sb.append(","+l);
			}
			st = conn.prepareStatement("delete from usuarios where id in(" + sb.toString() + ")");
			int linhasRemovidas = st.executeUpdate();
			if (linhasRemovidas == 0) {
				logger.log(Level.WARNING, "Nenhum registro foi removido.");
			}else {
				logger.log(Level.INFO, linhasRemovidas + " registro(s) removidos(s).");
			}
			return linhasRemovidas;
		}catch(SQLException | ClassNotFoundException ex) {
			if (ex instanceof SQLException) {
				throw new DAOException((SQLException)ex);
			}else {
				throw new DAOException((ClassNotFoundException)ex);
			}
		}finally {
			try {
				Conexao.getInstancia().close(rs, st, conn);
			}catch(SQLException | ClassNotFoundException ex) {
				if (ex instanceof SQLException) {
					throw new DAOException((SQLException)ex);
				}else {
					throw new DAOException((ClassNotFoundException)ex);
				}
			}
		}
	}

	private String buildSQL(Atributo... atributos) {
		StringBuilder sb = new StringBuilder();
		for (Atributo a : atributos) {
			Object valor = a.getValor();
			if (valor instanceof String || valor instanceof Double || valor instanceof Float) {
				sb.append(" and " + a.getNome() + " = '" + a.getValor() + "'");
			}
			if (valor instanceof Long || valor instanceof Integer || valor instanceof Short || valor instanceof Byte) {
				sb.append(" and " + a.getNome() + " = " + a.getValor());
			}
			if (valor instanceof Boolean) {
				if ((Boolean)a.getValor()) {
					sb.append(" and " + a.getNome() + " = 1");
				}else {
					sb.append(" and " + a.getNome() + " = 0");
				}
			}
		}
		return sb.toString();
	}
	
	@Override
	public List<Usuario> pesquisar(int limite, String orderBy, Atributo... atributos) throws DAOException {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		List<Usuario> resultado = new ArrayList<Usuario>();
		try {
			conn = Conexao.getInstancia().open();
			StringBuilder sb = new StringBuilder("select * from usuarios where 0 = 0");
			sb.append(this.buildSQL(atributos));
			if (orderBy!=null) {
				sb.append(" order by " + orderBy);
			}
			if (limite>0) {
				sb.append(" limit " + limite);
			}
			st = conn.createStatement();
			rs = st.executeQuery(sb.toString()); 
			while (rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(rs.getLong("id"));
				usuario.setNome(rs.getString("nome"));
				usuario.setLogin(rs.getString("login"));
				usuario.setSenha(rs.getString("senha"));
				usuario.setEmail(rs.getString("email"));
				resultado.add(usuario);
			}
			logger.log(Level.INFO, "Dados obtidos com sucesso.");
			return resultado;
		}catch(SQLException | ClassNotFoundException ex) {
			if (ex instanceof SQLException) {
				throw new DAOException((SQLException)ex);
			}else {
				throw new DAOException((ClassNotFoundException)ex);
			}
		}finally {
			try {
				Conexao.getInstancia().close(rs, st, conn);
			}catch(SQLException | ClassNotFoundException ex) {
				if (ex instanceof SQLException) {
					throw new DAOException((SQLException)ex);
				}else {
					throw new DAOException((ClassNotFoundException)ex);
				}
			}
		}		
	}

	/*
	@Override
	public List<Usuario> pesquisar(int limite, String orderBy, Atributo... atributos) throws DAOException {
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		List<Usuario> resultado = new ArrayList<Usuario>();
		try {
			conn = Conexao.getInstancia().open();
			StringBuilder sb = new StringBuilder("select * from usuarios where 0 = 0");
			for (int i = 0; i < atributos.length; i++) {
				sb.append(" and " + atributos[i].getNome() + " = ?");
			}
			if (orderBy!=null) {
				sb.append(" order by " + orderBy);
			}
			if (limite>0) {
				sb.append(" limit " + limite);
			}
			st = conn.prepareStatement(sb.toString());
			for (int i = 0; i < atributos.length; i++) {
				st.setObject(i+1, atributos[i].getValor());
			}
			rs = st.executeQuery();
			while (rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(rs.getLong("id"));
				usuario.setNome(rs.getString("nome"));
				usuario.setLogin(rs.getString("login"));
				usuario.setSenha(rs.getString("senha"));
				usuario.setEmail(rs.getString("email"));
				resultado.add(usuario);
			}
			logger.log(Level.INFO, "Dados obtidos com sucesso.");
			return resultado;
		}catch(SQLException | ClassNotFoundException ex) {
			if (ex instanceof SQLException) {
				throw new DAOException((SQLException)ex);
			}else {
				throw new DAOException((ClassNotFoundException)ex);
			}
		}finally {
			try {
				Conexao.getInstancia().close(rs, st, conn);
			}catch(SQLException | ClassNotFoundException ex) {
				if (ex instanceof SQLException) {
					throw new DAOException((SQLException)ex);
				}else {
					throw new DAOException((ClassNotFoundException)ex);
				}
			}
		}		
	}
	*/
	
	@Override
	public List<Usuario> pesquisar(String sql) throws DAOException {
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		List<Usuario> resultado = new ArrayList<Usuario>();
		try {
			conn = Conexao.getInstancia().open();
			st = conn.prepareStatement(sql);
			rs = st.executeQuery();
			while (rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(rs.getLong("id"));
				usuario.setNome(rs.getString("nome"));
				usuario.setLogin(rs.getString("login"));
				usuario.setSenha(rs.getString("senha"));
				usuario.setEmail(rs.getString("email"));
				resultado.add(usuario);
			}
			logger.log(Level.INFO, "Dados obtidos com sucesso.");
			return resultado;
		}catch(SQLException | ClassNotFoundException ex) {
			if (ex instanceof SQLException) {
				throw new DAOException((SQLException)ex);
			}else {
				throw new DAOException((ClassNotFoundException)ex);
			}
		}finally {
			try {
				Conexao.getInstancia().close(rs, st, conn);
			}catch(SQLException | ClassNotFoundException ex) {
				if (ex instanceof SQLException) {
					throw new DAOException((SQLException)ex);
				}else {
					throw new DAOException((ClassNotFoundException)ex);
				}
			}
		}
	}
}
