package br.upe.topicos.dao;

import java.io.Serializable;
import java.util.List;

import br.upe.topicos.dao.exceptions.DAOException;
import br.upe.topicos.entidade.Entidade;
import br.upe.topicos.entidade.usuario.Usuario;

public interface DAO<T extends Entidade<K>, K extends Serializable> {
	T inserir(T entidade) throws DAOException;
	T atualizar(T entidade) throws DAOException;
	int deletar(@SuppressWarnings("unchecked") K... ids) throws DAOException;
	List<Usuario> pesquisar(int limite, String orderBy, Atributo... atributos) throws DAOException;
	List<T> pesquisar(String sql) throws DAOException;
}
