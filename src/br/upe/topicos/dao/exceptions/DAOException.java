package br.upe.topicos.dao.exceptions;

import java.sql.SQLException;

public class DAOException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public DAOException(String msg) {
		super(msg);
	}

	public DAOException(Throwable t) {
		super(t);
	}

	public DAOException(String msg, Throwable t) {
		super(msg, t);
	}
	
	public DAOException(ClassNotFoundException ex) {
		super(ex);
	}

	public DAOException(SQLException ex) {
		super(ex);
	}
}