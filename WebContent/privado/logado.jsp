<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="br.upe.topicos.entidade.usuario.Usuario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!doctype html>
<html lang="en">
  <head>

    <title>Logado!</title>

  </head>
  
  <style>
  	.dashboard{
		font-size: 30px;
	}
  </style>
  
  <script>
  	function decrementarSessao(tempo){
  		
  		var date = new Date(null);
  		date.setSeconds(tempo); // specify value for SECONDS here
  		var timeString = date.toISOString().substr(11, 8);
  		var div = document.getElementById("cronometro").innerHTML=timeString;
  		setTimeout('decrementarSessao(' + (tempo-1).toFixed(0) + ')',1000);
  		if (tempo<=0){
  			location.href='logado.jsp';
  		}
  	}
  </script>


	<%
		if (session.getAttribute("logado") == null) {
			session.setAttribute("msg", "Sua sess�o foi expirada!");
			response.sendRedirect("/LoginBasico/index.jsp");
		}
	
		if (session.getAttribute("logado") != null) {
			Usuario u = (Usuario)session.getAttribute("logado");
			
			//Long segundos = session.getMaxInactiveInterval() - (System.currentTimeMillis() - session.getLastAccessedTime()) / 1000;
			
			//long minutosRestantes = TimeUnit.SECONDS.toMinutes(segundos);
			
			out.print("<div class='dashboard'>Bem vindo, " + u.getNome() + "!");
			out.print("<BR>Tempo restantes da sess�o: <div id='cronometro'></div>");
			out.print("<BR><a href='/LoginBasico/deslogar'>Deslogar</a></div>");			
		}
		
		//session.setAttribute("logado", null);
	%>


  <body onload="decrementarSessao('<%=session.getMaxInactiveInterval() - (System.currentTimeMillis() - session.getLastAccessedTime()) / 1000%>');">
  
  </body>
</html>
