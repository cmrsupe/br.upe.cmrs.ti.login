<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Seja bem vindo, visitante!</title>

<link href="assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<link rel="stylesheet" type="text/css" href="assets/css/exemplo.css">

</head>

<body onload="document.form1.login.focus();">

	<%
		/*
			if (session.getAttribute("logado") != null) {
				response.sendRedirect("privado/logado.jsp");
			}else{
				if (session.getAttribute("msg") != null) {
					out.print("<center><h1>"+ session.getAttribute("msg")+"</h1></center><BR>");
					session.setAttribute("msg", null);
				}
			}
		*/
	%>

	<c:choose>
		<c:when test="${ logado ne null }">
			<jsp:forward page="privado/logado.jsp" />
		</c:when>
		<c:when test="${ msg ne null }">
			<c:out value="<center><h1>${msg}</h1></center><BR>" escapeXml="false" />
		</c:when>
	</c:choose>


	<form name="form1" method="post" action="autenticar">
		<table align="center" class='formulario'>
			<tr>
				<td>Login:</td>
				<td><input type="text" name="login"></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Senha:</td>
				<td><input type="password" name="senha"></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit"
					name="confirmar" value=" Autenticar "></td>
			</tr>
		</table>
	</form>

</body>
</html>